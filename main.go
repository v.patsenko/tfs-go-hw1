package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

func main() {
	var tikerNameSlice = []string{"AAPL", "AMZN", "SBER"}
	// Map with MaxPrice, MinPrice and MaxRevenue for each tiker.
	// Values are accessible via tikerPriceMap[tikerName][MaxPrice]
	var tikerPriceMap = map[string]map[string]float64{}

	for _, tikerName := range tikerNameSlice {
		max, min := getMaxAndMinForTiker(tikerName)
		tikerPriceMap[tikerName] = map[string]float64{}

		tikerPriceMap[tikerName]["MaxPrice"] = max
		tikerPriceMap[tikerName]["MinPrice"] = min
		tikerPriceMap[tikerName]["MaxRevenue"] = max - min
	}
	// user revenue groped by tikerName   revenue for tiker - userRevenuesGroupedByTiker[tikerName][UserId]

	var userRevenuesGroupedByTiker = make(map[string]map[string]float64)

	for _, tikerName := range tikerNameSlice {
		userRevenuesGroupedByTiker[tikerName] = make(map[string]float64)
		userRevenuesGroupedByTiker[tikerName] = countUserRevenue(tikerName)
	}

	// Dates to buy and sell tiker datesBuySellForTiker[tikerName][Buy]
	var datesBuySellForTiker = make(map[string]map[string]string)

	datesBuySellForTiker["AAPL"] = make(map[string]string)
	datesBuySellForTiker["AMZN"] = make(map[string]string)
	datesBuySellForTiker["SBER"] = make(map[string]string)

	var linesToWriteSlice = make([][]string, 0)

	for _, tikerName := range tikerNameSlice {
		max, min := getMaxAndMinForTiker(tikerName)
		buyDate, sellDate := getTime(tikerName, max, min)
		datesBuySellForTiker[tikerName]["Buy"] = buyDate
		datesBuySellForTiker[tikerName]["Sell"] = sellDate
	}

	for _, tikerName := range tikerNameSlice {
		for id, userRevenues := range userRevenuesGroupedByTiker[tikerName] {
			var line = make([]string, 0)
			line = append(line, id)
			line = append(line, tikerName)

			userRevenueString := fmt.Sprintf("%.2f", userRevenues)
			maxRevenue := fmt.Sprintf("%.2f", tikerPriceMap[tikerName]["MaxRevenue"])

			lostRevenue := fmt.Sprintf("%.2f", tikerPriceMap[tikerName]["MaxRevenue"]-userRevenues)

			line = append(line, userRevenueString)
			line = append(line, maxRevenue)
			line = append(line, lostRevenue)
			line = append(line, datesBuySellForTiker[tikerName]["Buy"])
			line = append(line, datesBuySellForTiker[tikerName]["Sell"])

			linesToWriteSlice = append(linesToWriteSlice, line)
		}
	}

	writeCSV(linesToWriteSlice, "Output.csv")
}

func getTime(tikerName string, highPrice float64, lowPrice float64) (string, string) {
	csvfile, err := os.Open("candles_5m.csv")
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	r := csv.NewReader(csvfile)

	highPriceStirng := fmt.Sprintf("%.2f", highPrice)
	lowPriceString := fmt.Sprintf("%.2f", lowPrice)

	highPriceStirng = strings.TrimRight(strings.TrimRight(highPriceStirng, "0"), ".")
	lowPriceString = strings.TrimRight(strings.TrimRight(lowPriceString, "0"), ".")

	var (
		buyDate  string
		sellDate string
	)

	for {
		row, err := r.Read()

		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatal(err)
		}

		if row[0] == tikerName {
			if row[3] == highPriceStirng {
				sellDate = row[1]
			}

			if row[4] == lowPriceString {
				buyDate = row[1]
			}
		}
	}

	return buyDate, sellDate
}

// Read CSV file and push high and low price into slice and
//then search for min and max value in slice and return them
func getMaxAndMinForTiker(tikerName string) (max float64, min float64) {
	// Чтение CSV файла
	csvfile, err := os.Open("candles_5m.csv")
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	r := csv.NewReader(csvfile)

	var tikerHighPrice []float64

	var tikerLowPrice []float64

	for {
		//  Получение строки
		tikerRow, err := r.Read()

		// Если input закончился, то цикл останавливается
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatal(err)
		}

		if tikerRow[0] == tikerName {
			// Convert string into float32 and append value to appleHighPrice slice
			if value, err := strconv.ParseFloat(tikerRow[3], 32); err == nil {
				tikerHighPrice = append(tikerHighPrice, value)
			}

			// Convert string into float32 and append value to appleLowPrice slice
			if value, err := strconv.ParseFloat(tikerRow[4], 32); err == nil {
				tikerLowPrice = append(tikerLowPrice, value)
			}
		}
	}

	max = getMax(tikerHighPrice)
	min = getMin(tikerLowPrice)

	return max, min
}

// Linear search of max value in slice
func getMax(slice []float64) float64 {
	sliceMaxValue := slice[0]

	for i := 0; i < len(slice)-1; i++ {
		k := i
		k++

		if sliceMaxValue < slice[k] {
			sliceMaxValue = slice[k]
		}
	}

	return sliceMaxValue
}

// Linear search of min value in slice
func getMin(slice []float64) float64 {
	sliceMinValue := slice[0]

	for i := 0; i < len(slice)-1; i++ {
		k := i
		k++

		if sliceMinValue > slice[k] {
			sliceMinValue = slice[k]
		}
	}

	return sliceMinValue
}

// Get revenue for each user
func countUserRevenue(tikerName string) map[string]float64 {
	csvfile, err := os.Open("user_trades.csv")

	checkErr("Couldn't open the csv file", err)

	r := csv.NewReader(csvfile)

	dealBuyPrice := make(map[string]string)
	dealSellPrice := make(map[string]string)

	for {
		userDeals, err := r.Read()

		if err == io.EOF {
			break
		}

		checkErr("error while reading csv", err)

		if userDeals[3] != "0" && userDeals[2] == tikerName {
			dealBuyPrice[userDeals[0]] = userDeals[3]
		}

		if userDeals[4] != "0" && userDeals[2] == tikerName {
			dealSellPrice[userDeals[0]] = userDeals[4]
		}
	}

	userIDRevenueMap := make(map[string]float64)

	for id := range dealBuyPrice {
		var userPriceBuy float64

		var userPriceSell float64

		if sell, err := strconv.ParseFloat(dealSellPrice[id], 64); err == nil {
			userPriceSell = sell
		}

		if buy, err := strconv.ParseFloat(dealBuyPrice[id], 64); err == nil {
			userPriceBuy = buy
		}

		userRevenue := userPriceSell - userPriceBuy

		userIDRevenueMap[id] = toFixed(userRevenue, 2)
	}

	return userIDRevenueMap
}

func checkErr(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

func round(num float64) int {
	x := 0.5
	return int(num + math.Copysign(x, num))
}

func toFixed(num float64, precision int) float64 {
	x := 10.0
	output := math.Pow(x, float64(precision))

	return float64(round(num*output)) / output
}

func writeCSV(linesToWrite [][]string, filename string) {
	file, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for _, value := range linesToWrite {
		err := writer.Write(value)
		if err != nil {
			log.Fatal(err)
		}
	}
}
